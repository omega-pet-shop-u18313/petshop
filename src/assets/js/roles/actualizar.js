function actualizarArt(){
    const codigo = document.getElementById("cod").value;
    const nombre = document.getElementById("nom").value;
  if (nombre !== "") {
    let objetoEnviar = {
        idArtista: codigo,
        nombreArtista: nombre,
    };
    const apiCrear = "http://localhost:8094/rockola/artistas/actualizar";
    fetch(apiCrear, {
      method: "PUT",
      body: JSON.stringify(objetoEnviar),
      headers: {"Content-type": "application/json; charset=UTF-8" },
    })
      .then((respuesta) => respuesta.json())
      .then((datos) =>{
        if (datos.hasOwnProperty("idArtista")) {
          document.getElementById("rolMsgOk").style.display = "";
          document.getElementById("rolMsgError").style.display = "none";
        } else {
          document.getElementById("rolMsgOk").style.display = "none";
          document.getElementById("rolMsgError").style.display = "";
        }
        window.location.replace("#rolmanage");
    })
      .catch((miError) => console.log(miError));
  }
}