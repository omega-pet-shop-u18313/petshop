function crearRol(){
    const nombre = document.getElementById("nom").value;
    if(nombre!==""){
        let objetoEnviar = {
            nombreArtista:nombre,
        };
        const apiCrear = "http://localhost:8094/rockola/artistas/crear";
        fetch(apiCrear,{
            method:"POST",
            body:JSON.stringify(objetoEnviar),
            headers:{"Content-type":"application/json; charset=UTF-8"}
        }).then(respuesta=>respuesta.json()).then(datos=>{
            if(datos.hasOwnProperty("idArtista")){
                document.getElementById("rolMsgOk").style.display ="";
                document.getElementById("rolMsgError").style.display ="none";
            }else{
                document.getElementById("rolMsgOk").style.display ="none";
                document.getElementById("rolMsgError").style.display ="";
            }
        }
        ).catch(miError=>console.log(miError));
        document.getElementById("formaRol").reset();
        document.getElementById("formaRol").classList.remove("was-valitated");
    } 
}