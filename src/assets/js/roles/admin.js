function obtenerTodosArtAdmin(){
    const apiObtenerArtistas = "http://localhost:8094/rockola/artistas/todos";
    const miPromesaArtistas = fetch(apiObtenerArtistas).then((respuesta) =>
    respuesta.json()
    );

    Promise.all([miPromesaArtistas]).then((arregloDatos) => {
        const datoss = arregloDatos[0];
        crearFilasArtistas(datoss);
    });
}

function crearFilasArtistas(arregloObjeto) {
    const cantidad = arregloObjeto.length;
    for (let i = 0; i < cantidad; i++) {
        const codArt = arregloObjeto[i].idArtista;
        const nomArt = arregloObjeto[i].nombreArtista;

        document.getElementById("tablaRolesAdmin").insertRow(-1).innerHTML =
        "<td>" + codArt + "</td>" + "<td>" + nomArt + "</td>"
        +"<td class='text-center'>" + "<a href='javascript:confirmarArtEliminar'("+codArt+");'><i class='fa-solid fa-trash-can check-rojo'></i></a>&nbsp;" 
        + "<a href='javascript:#rolupdate/"+codArt+"'><i class='fa-solid fa-pen-to-square'></i></a>"
        + "</td>";
    }
}

function confirmarArtEliminar(codigo){
    if(window.confirm("estas seguro que quieres borrar el artista?")){
        window.location.replace("#roldelete/" + codigo);
    }
}
