function eliminarArt(parametro){
    const apiObtenerArtistas = "http://localhost:8094/rockola/artistas/borrar/"+parametro;
  const miPromesaArtistas = fetch(apiObtenerArtistas, {method:'DELETE'}).then((respuesta) => 
  respuesta.json()
  );

  Promise.all([miPromesaArtistas]).then((arregloDatos) => {
    const datos = arregloDatos[0];
    if (datos.status=="200") {
        document.getElementById('alertRolEliminar').classList.add('alert-primary');
        document.getElementById('msgRolEliminar').innerHTML="El artista ha sido eliminado";
    } else {
        document.getElementById('alertRolEliminar').classList.add('alert-danger');
        document.getElementById('msgRolEliminar').innerHTML="El artista no pudo ser eliminado";
    }
  });
}