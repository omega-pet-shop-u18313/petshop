function obtenerTodosRoles(){
    const apiObtenerRoles ="http://localhost:8094/rockola/artistas/todos";
    const miPromesaRoles = fetch(apiObtenerRoles).then((respuesta)=>respuesta.json());

    Promise.all([miPromesaRoles]).then(
        (arregloDatos)=>{
            const datos = arregloDatos[0];
            // ya se conecto el backend con el front
            crearFilas(datos);
        }
    );
}

function crearFilas(arrObjeto){
    const cantidad = arrObjeto.length;
    for(let i = 0;i<cantidad; i++){
        const codRoll = arrObjeto[i].idArtista;
        const nomRoll = arrObjeto[i].nombreArtista;

        document.getElementById("tablaRoles").insertRow(-1).innerHTML = "<td>"+codRoll+"</td>" + "<td>"+nomRoll+"</td>";

    }
}